#include "PaintScene.h"

PaintScene::PaintScene(QObject *parent) :
    QGraphicsScene(parent)
{
}

void PaintScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsScene::mouseReleaseEvent(event);
    emit this->mouseRelease(event);
}

void PaintScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsScene::mousePressEvent(event);
    emit this->mousePress(event);
}

void PaintScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsScene::mouseMoveEvent(event);
    emit this->mouseMove(event);
}
