#include "PaintConfiguration.h"

PaintConfiguration::PaintConfiguration(QWidget *parent) :
    QWidget(parent)
{
    this->shape = new ShapeConfiguration();
    this->penWidth = new PenWidthConfiguration();
    this->color = new ColorConfiguration();
    this->brush = new BrushConfiguration();

    auto layout = new QHBoxLayout();
    layout->addWidget(this->shape);
    layout->addWidget(this->penWidth);
    layout->addWidget(this->color);
    layout->addWidget(this->brush);
    layout->addStretch();

    this->setLayout(layout);
}

ShapeConfiguration* PaintConfiguration::getShape()
{
    return this->shape;
}

PenWidthConfiguration* PaintConfiguration::getPenWidth()
{
    return this->penWidth;
}

ColorConfiguration *PaintConfiguration::getPenColor()
{
    return this->color;
}

BrushConfiguration *PaintConfiguration::getPenBrush()
{
    return this->brush;
}

QPen PaintConfiguration::getPen()
{
    QPen pen;
    pen.setWidthF(this->getPenWidth()->getValue());
    pen.setBrush(QBrush(this->getPenColor()->getColor(),this->getPenBrush()->getBrush().style()));
    return pen;
}
