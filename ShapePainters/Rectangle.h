#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "Abstract.h"

class RectanglePainter : public AbstractShapePainter
{
    Q_OBJECT
public:
    explicit RectanglePainter();
    virtual QGraphicsRectItem* getShape();

protected:

    virtual void buildShape(QGraphicsSceneMouseEvent *e);
    virtual void setupShape();
    virtual void refreshShape(QGraphicsSceneMouseEvent *e);

};

#endif // RECTANGLE_H
