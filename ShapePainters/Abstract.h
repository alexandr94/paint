#ifndef ABSTRACT_SHAPE_PAINTER_H
#define ABSTRACT_SHAPE_PAINTER_H

#include <QDebug>
#include <QGraphicsLineItem>
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsTextItem>

#include "PaintScene.h"
#include "PaintConfiguration.h"

class AbstractShapePainter : public QObject
{
    Q_OBJECT
public:
    explicit AbstractShapePainter();

    virtual void listenEvents();
    virtual void stopListeningEvents();

    virtual void setScene(PaintScene *newScene);
    virtual void setConfiguration(PaintConfiguration *cfg);

protected:

    QPointF point;

    PaintScene *scene;
    QGraphicsItem *shape;
    PaintConfiguration *config;

    bool isListening;

    virtual void buildShape(QGraphicsSceneMouseEvent *e) = 0;

    virtual void setupShape();
    virtual void paintShape(QGraphicsSceneMouseEvent *e);
    virtual void refreshShape(QGraphicsSceneMouseEvent *e);

    virtual void prepareTemporaries(QGraphicsSceneMouseEvent *e);
    virtual void updateTemporaries(QGraphicsSceneMouseEvent *e);
    virtual void clearTemporaries();

    virtual bool shouldBeginPainting(QGraphicsSceneMouseEvent *e);
    virtual bool shouldContinuePainting(QGraphicsSceneMouseEvent *e);
    virtual bool shouldStopPainting(QGraphicsSceneMouseEvent *e);

    virtual bool isPainting();

    virtual QGraphicsItem* getShape();

    virtual ~AbstractShapePainter(){}

protected slots:

    void onMousePress(QGraphicsSceneMouseEvent *e);
    void onMouseMove(QGraphicsSceneMouseEvent *e);
    void onMouseRelease(QGraphicsSceneMouseEvent *e);

};

#endif // ABSTRACT_SHAPE_PAINTER_H
