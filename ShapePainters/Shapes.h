#ifndef SHAPES_H
#define SHAPES_H

enum Shapes
{
    LINE = 1,
    ELLIPSE = 2,
    RECTANGLE = 3
};

#endif // SHAPES_H
