#ifndef ELLIPSE_H
#define ELLIPSE_H

#include <PaintScene.h>
#include "Abstract.h"

class EllipsePainter : public AbstractShapePainter
{
    Q_OBJECT
public:
    explicit EllipsePainter();

protected:

    virtual void buildShape(QGraphicsSceneMouseEvent *e);
    virtual void setupShape();
    virtual void refreshShape(QGraphicsSceneMouseEvent *e);

    virtual QGraphicsEllipseItem* getShape();

};

#endif // ELLIPSE_H
