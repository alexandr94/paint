#include "Ellipse.h"

EllipsePainter::EllipsePainter() :
    AbstractShapePainter()
{
}

void EllipsePainter::buildShape(QGraphicsSceneMouseEvent *e)
{
    this->shape = new QGraphicsEllipseItem(QRectF(e->scenePos(), e->scenePos()));
}

void EllipsePainter::refreshShape(QGraphicsSceneMouseEvent *e)
{
    QRectF pos(this->point, e->scenePos());
    this->getShape()->setRect(pos);
}


QGraphicsEllipseItem* EllipsePainter::getShape()
{
    return static_cast<QGraphicsEllipseItem*>(AbstractShapePainter::getShape());
}

void EllipsePainter::setupShape()
{
    if (this->config) {
        this->getShape()->setPen(this->config->getPen());
    }
}
