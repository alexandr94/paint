#include "Abstract.h"

AbstractShapePainter::AbstractShapePainter()
{
    this->isListening = false;
    this->scene = nullptr;
    this->shape = nullptr;
}

void AbstractShapePainter::listenEvents()
{
    this->stopListeningEvents();
    if (this->scene) {
        connect(this->scene, SIGNAL(mousePress(QGraphicsSceneMouseEvent*)), this, SLOT(onMousePress(QGraphicsSceneMouseEvent*)));
        connect(this->scene, SIGNAL(mouseMove(QGraphicsSceneMouseEvent*)), this, SLOT(onMouseMove(QGraphicsSceneMouseEvent*)));
        connect(this->scene, SIGNAL(mouseRelease(QGraphicsSceneMouseEvent*)), this, SLOT(onMouseRelease(QGraphicsSceneMouseEvent*)));
        this->isListening = true;
    }
}

void AbstractShapePainter::stopListeningEvents()
{
    if (this->scene && this->isListening) {
        disconnect(this->scene, SIGNAL(mousePress(QGraphicsSceneMouseEvent*)), this, SLOT(onMousePress(QGraphicsSceneMouseEvent*)));
        disconnect(this->scene, SIGNAL(mouseMove(QGraphicsSceneMouseEvent*)), this, SLOT(onMouseMove(QGraphicsSceneMouseEvent*)));
        disconnect(this->scene, SIGNAL(mouseRelease(QGraphicsSceneMouseEvent*)), this, SLOT(onMouseRelease(QGraphicsSceneMouseEvent*)));
        this->isListening = false;
    }
}

void AbstractShapePainter::setScene(PaintScene *newScene)
{
    if (this->scene != newScene) {
        this->stopListeningEvents();
        this->scene = newScene;
        this->setParent(newScene);
    }
}

void AbstractShapePainter::setConfiguration(PaintConfiguration *cfg)
{
    this->config = cfg;
}

bool AbstractShapePainter::shouldBeginPainting(QGraphicsSceneMouseEvent *e)
{
    return e->button() == Qt::LeftButton;
}

bool AbstractShapePainter::shouldContinuePainting(QGraphicsSceneMouseEvent *e)
{
    return (e->buttons() & Qt::LeftButton) && this->isPainting();
}

bool AbstractShapePainter::shouldStopPainting(QGraphicsSceneMouseEvent *e)
{
    return e->button() == Qt::LeftButton && this->isPainting();
}

bool AbstractShapePainter::isPainting()
{
    return this->shape != nullptr;
}

 QGraphicsItem *AbstractShapePainter::getShape()
{
    return this->shape;
}

void AbstractShapePainter::onMousePress(QGraphicsSceneMouseEvent *e)
{
    if (this->shouldBeginPainting(e)) {
        this->prepareTemporaries(e);
        this->buildShape(e);
        this->setupShape();
        this->paintShape(e);
    }
}

void AbstractShapePainter::onMouseMove(QGraphicsSceneMouseEvent *e)
{
    if (this->shouldContinuePainting(e)) {
        this->updateTemporaries(e);
        this->refreshShape(e);
    }
}

void AbstractShapePainter::onMouseRelease(QGraphicsSceneMouseEvent *e)
{
    if (this->shouldStopPainting(e)) {
        this->updateTemporaries(e);
        this->refreshShape(e);
        this->clearTemporaries();
    }
}

void AbstractShapePainter::paintShape(QGraphicsSceneMouseEvent *e)
{
    this->scene->addItem(this->getShape());
}

void AbstractShapePainter::refreshShape(QGraphicsSceneMouseEvent *e)
{

}

void AbstractShapePainter::setupShape()
{

}

void AbstractShapePainter::prepareTemporaries(QGraphicsSceneMouseEvent *e)
{
    this->point = e->scenePos();
}

void AbstractShapePainter::updateTemporaries(QGraphicsSceneMouseEvent *e)
{

}

void AbstractShapePainter::clearTemporaries()
{
    this->point.setX(0);
    this->point.setY(0);
    this->shape = nullptr;
}
