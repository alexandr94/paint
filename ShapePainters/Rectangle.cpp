#include "Rectangle.h"

RectanglePainter::RectanglePainter() :
    AbstractShapePainter()
{
}

void RectanglePainter::buildShape(QGraphicsSceneMouseEvent *e)
{
    this->shape = new QGraphicsRectItem(QRectF(e->scenePos(), e->scenePos()));
}

void RectanglePainter::refreshShape(QGraphicsSceneMouseEvent *e)
{
    QRectF pos(this->point, e->scenePos());
    this->getShape()->setRect(pos);
}

QGraphicsRectItem *RectanglePainter::getShape()
{
    return static_cast<QGraphicsRectItem*>(AbstractShapePainter::getShape());
}

void RectanglePainter::setupShape()
{
    if (this->config) {
        this->getShape()->setPen(this->config->getPen());
    }
}
