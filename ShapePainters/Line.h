#ifndef LINE_PAINTER_H
#define LINE_PAINTER_H

#include "Abstract.h"

class LinePainter : public AbstractShapePainter
{
    Q_OBJECT
public:
    explicit LinePainter();

protected:

    virtual void buildShape(QGraphicsSceneMouseEvent *e);
    virtual void setupShape();
    virtual void refreshShape(QGraphicsSceneMouseEvent *e);

    virtual QGraphicsLineItem *getShape();

};

#endif // LINE_PAINTER_H
