#ifndef FACTORY_H
#define FACTORY_H

#include <unordered_map>

#include "Ellipse.h"
#include "Line.h"
#include "Rectangle.h"
#include "Shapes.h"


class PainterFactory
{
public:
    PainterFactory();

    AbstractShapePainter* getBy(int shape);

private:

    std::unordered_map<int, AbstractShapePainter*> cachedPainters;

};

#endif // FACTORY_H
