#include "Factory.h"

PainterFactory::PainterFactory()
{
}

AbstractShapePainter* PainterFactory::getBy(int shape)
{
    AbstractShapePainter *painter;
    switch (shape)
    {
        case Shapes::ELLIPSE:
            painter = this->cachedPainters[shape];
            if (!painter) {
                painter = new EllipsePainter();
                this->cachedPainters[shape] = painter;
            }
            return static_cast<EllipsePainter*>(painter);

        case Shapes::RECTANGLE:
            painter = this->cachedPainters[shape];
            if (!painter) {
                painter = new RectanglePainter();
                this->cachedPainters[shape] = painter;
            }
            return static_cast<RectanglePainter*>(painter);

        default:
            painter = this->cachedPainters[shape];
            if (!painter) {
                painter = new LinePainter();
                this->cachedPainters[shape] = painter;
            }
            return static_cast<LinePainter*>(painter);;
    }
}
