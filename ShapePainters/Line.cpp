#include "Line.h"
#include <QDebug>

LinePainter::LinePainter() :
    AbstractShapePainter()
{
}

void LinePainter::buildShape(QGraphicsSceneMouseEvent *e)
{
    this->shape = new QGraphicsLineItem(QLineF(e->scenePos(), e->scenePos()));
}

void LinePainter::refreshShape(QGraphicsSceneMouseEvent *e)
{
    QLineF pos(this->point, e->scenePos());
    this->getShape()->setLine(pos);
}

QGraphicsLineItem* LinePainter::getShape()
{
    return static_cast<QGraphicsLineItem*>(AbstractShapePainter::getShape());
}

void LinePainter::setupShape()
{
    if (this->config) {
        this->getShape()->setPen(this->config->getPen());
    }
}
