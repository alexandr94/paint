#ifndef PAINTCONFIGURATION_H
#define PAINTCONFIGURATION_H

#include <QWidget>
#include <QHBoxLayout>
#include <QPen>

#include "PaintConfigurationItems/Shape.h"
#include "PaintConfigurationItems/PenWidth.h"
#include "PaintConfigurationItems/Color.h"
#include "PaintConfigurationItems/Brush.h"

class PaintConfiguration : public QWidget
{
    Q_OBJECT
public:
    explicit PaintConfiguration(QWidget *parent = 0);

    ShapeConfiguration* getShape();
    PenWidthConfiguration* getPenWidth();
    ColorConfiguration* getPenColor();
    BrushConfiguration* getPenBrush();

    QPen getPen();

protected:
    ShapeConfiguration *shape;
    PenWidthConfiguration *penWidth;
    ColorConfiguration *color;
    BrushConfiguration *brush;

};

#endif // PAINTCONFIGURATION_H
