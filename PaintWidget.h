#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QGraphicsView>
#include <QGLWidget>
#include <QMouseEvent>
#include <QFileDialog>
#include <QMessageBox>
#include <QApplication>

#include "ShapePainters/Abstract.h"
#include "PaintConfiguration.h"
#include "ShapePainters/Factory.h"
#include "ShapePainters/Shapes.h"

class PaintWidget : public QGraphicsView
{
    Q_OBJECT
public:
    explicit PaintWidget(QWidget *parent = 0);

    void installScene(PaintScene *scene);
    void setConfiguration(PaintConfiguration *config);

    virtual void saveSceneImage();
    virtual void clearScene();
    virtual void quit();

    ~PaintWidget();

protected:

    PaintScene *scene;
    AbstractShapePainter *activePainter;
    PaintConfiguration *config;
    PainterFactory *factory;

    void setupRenderer();
    void setupViewport();

    virtual void resizeEvent(QResizeEvent *event);

protected slots:

    void changeActivePainter(int shape);

};

#endif // PAINTWIDGET_H
