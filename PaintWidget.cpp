#include "PaintWidget.h"

PaintWidget::PaintWidget(QWidget *parent) :
    QGraphicsView(parent)
{
    this->activePainter = nullptr;
    this->scene = nullptr;
    this->config = nullptr;
    this->factory = nullptr;
    setupRenderer();
    setupViewport();
}

void PaintWidget::installScene(PaintScene *scene)
{
    this->scene = scene;
    this->setScene(scene);
}

void PaintWidget::setupRenderer()
{
    setCacheMode(QGraphicsView::CacheBackground);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    auto flags = QGraphicsView::DontAdjustForAntialiasing | QGraphicsView::DontSavePainterState;
    setOptimizationFlags(flags);
    auto hints = QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::HighQualityAntialiasing;
    setRenderHints(hints);
    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    this->setAlignment(Qt::AlignCenter);
    this->setMouseTracking(true);
}

void PaintWidget::setupViewport()
{
    QGLFormat format;
    format.setAccum(false);
    format.setAlpha(false);
    format.setDepth(false);
    format.setDirectRendering(true);
    format.setDoubleBuffer(true);
    format.setOverlay(false);
    format.setSampleBuffers(true);
    format.setStencil(false);
    format.setStereo(false);
    setViewport( new QGLWidget( format ) );
}

void PaintWidget::setConfiguration(PaintConfiguration *config)
{
    this->config = config;
    connect(config->getShape(), SIGNAL(shapeChangeEvent(int)), this, SLOT(changeActivePainter(int)));
    this->changeActivePainter(this->config->getShape()->getValue());
}

void PaintWidget::changeActivePainter(int shape)
{
    if (this->activePainter) {
        this->activePainter->stopListeningEvents();
    }
    if (!this->factory) {
        this->factory = new PainterFactory();
    }
    this->activePainter = this->factory->getBy(shape);
    this->activePainter->setScene(this->scene);
    this->activePainter->setConfiguration(this->config);
    this->activePainter->listenEvents();
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
    QGraphicsView::resizeEvent(event);
    if (this->scene) {
        this->scene->setSceneRect(0, 0, width() - 2, height() - 2);
    }
}

void PaintWidget::saveSceneImage()
{
    QString selfilter = tr("JPEG (*.jpg *.jpeg)");
    QString fileName = QFileDialog::getSaveFileName(0,"Saving picture",
            "D:\"",
            tr("All files (*.*);;JPEG (*.jpg *.jpeg);;TIFF (*.tif)" ),
            &selfilter
    );

    if (!fileName.isEmpty()){
        QImage image(this->scene->width(), this->scene->height(), QImage::Format_ARGB32_Premultiplied);
        image.fill(Qt::white);
        QPainter painter(&image);
        this->scene->render(&painter);
        image.save(fileName);
    }
}

void PaintWidget::clearScene()
{
    this->scene->clear();
}

void PaintWidget::quit()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Quit", "Save picture?",
                                  QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
    if (reply == QMessageBox::Yes) {
      this->saveSceneImage();
      QApplication::quit();
    }
    if(reply == QMessageBox::Cancel){

    }
    else {
      QApplication::quit();
    }
}

PaintWidget::~PaintWidget()
{
    if (this->factory) {
        delete this->factory;
    }
}
