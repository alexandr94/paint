#include "MainWindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(save()));
    connect(ui->actionClear, SIGNAL(triggered()), this, SLOT(clear()));
    connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(quit()));

    this->setWindowTitle("Paint");

    this->paintWidget = new PaintWidget();
    this->scene = new PaintScene();
    auto config = new PaintConfiguration();

    this->paintWidget->installScene(this->scene);
    this->paintWidget->setConfiguration(config);

    auto widget = new QWidget();
    auto vLayout = new QVBoxLayout();

    vLayout->addWidget(config);
    vLayout->addWidget(this->paintWidget);


    widget->setLayout(vLayout);
    this->setCentralWidget(widget);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::save()
{
  this->paintWidget->saveSceneImage();
}

void MainWindow::clear()
{
    this->paintWidget->clearScene();
}

void MainWindow::quit()
{
  this->paintWidget->quit();
}
