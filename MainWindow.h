#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QMessageBox>

#include <PaintScene.h>
#include <PaintWidget.h>
#include <ShapePainters/Line.h>
#include <ShapePainters/Rectangle.h>
#include <PaintConfiguration.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void save();
    void clear();
    void quit();

private:
    Ui::MainWindow *ui;

protected:
    PaintScene *scene;
    PaintWidget *paintWidget;

};

#endif // MAINWINDOW_H
