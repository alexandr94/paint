QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Paint
TEMPLATE = app


SOURCES += main.cpp\
    ShapePainters/Abstract.cpp \
    PaintWidget.cpp \
    ShapePainters/Line.cpp \
    PaintScene.cpp \
    ShapePainters/Ellipse.cpp \
    ShapePainters/Rectangle.cpp \
    MainWindow.cpp \
    PaintConfigurationItems/Shape.cpp \
    ShapePainters/Factory.cpp \
    PaintConfigurationItems/PenWidth.cpp \
    PaintConfiguration.cpp \
    PaintConfigurationItems/Color.cpp \
    PaintConfigurationItems/Brush.cpp

HEADERS  += \
    ShapePainters/Abstract.h \
    PaintWidget.h \
    ShapePainters/Line.h \
    PaintScene.h \
    ShapePainters/Ellipse.h \
    ShapePainters/Rectangle.h \
    PaintConfiguration.h \
    MainWindow.h \
    PaintConfigurationItems/Shape.h \
    ShapePainters/Shapes.h \
    ShapePainters/Factory.h \
    PaintConfigurationItems/PenWidth.h \
    PaintConfigurationItems/Color.h \
    PaintConfigurationItems/Brush.h \
    PaintConfigurationItems/Brushes.h

FORMS    += mainwindow.ui

CONFIG += c++11
