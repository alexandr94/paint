#ifndef BRUSH_H
#define BRUSH_H

#include <QComboBox>
#include <QBrush>

#include "Brushes.h"

class BrushConfiguration : public QComboBox
{
    Q_OBJECT
public:
    explicit BrushConfiguration(QWidget *parent = 0);

    int getValue();
    QBrush getBrush();
    void setBrush(QBrush brush);

signals:
    void brushChangeEvent(int shape);

protected slots:
    void onBrushChanged(int index);

protected:
   QBrush brush;

};

#endif // BRUSH_H
