#include "Shape.h"

ShapeConfiguration::ShapeConfiguration(QWidget *parent) :
    QComboBox(parent)
{
    this->addItem("Line", QVariant(Shapes::LINE));
    this->addItem("Ellipse", QVariant(Shapes::ELLIPSE));
    this->addItem("Rectangle", QVariant(Shapes::RECTANGLE));
    connect(this, SIGNAL(currentIndexChanged(int)), this, SLOT(onShapeChanged(int)));
}

void ShapeConfiguration::onShapeChanged(int index)
{
    emit this->shapeChangeEvent(this->itemData(index).toInt());
}

int ShapeConfiguration::getValue()
{
    return this->currentData().toInt();
}
