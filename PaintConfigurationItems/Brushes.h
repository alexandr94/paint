#ifndef BRUSHES_H
#define BRUSHES_H

enum Brushes
{
    SOLID_LINE = 0,
    POINTS_LINE = 1,
    HORIZONTAL_LINE = 2,
    VERTIVAL_LINE = 3
};

#endif // BRUSHES_H
