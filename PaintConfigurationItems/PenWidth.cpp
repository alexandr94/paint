#include "PenWidth.h"

PenWidthConfiguration::PenWidthConfiguration(QWidget *parent) :
    QDoubleSpinBox(parent)
{
    this->setSingleStep(0.5);
    this->setValue(1.0);
}

double PenWidthConfiguration::getValue()
{
    return this->value();
}
