#include "Color.h"

ColorConfiguration::ColorConfiguration(QWidget *parent) :
    QWidget(parent)
{
    this->color = Qt::black;

    this->lblCurrentColor = new QLabel();
    this->lblCurrentColor->setAutoFillBackground(true);
    this->lblCurrentColor->setFixedSize(16, 16);
    setLabelColor(this->getColor());

    auto colorLayout = new QHBoxLayout();

    auto pbChooseColor = new QPushButton();
    pbChooseColor->setText("Choose color");
    connect(pbChooseColor, SIGNAL(clicked()), this, SLOT(onChooseButtonClicked()));

    colorLayout->addWidget(pbChooseColor);
    colorLayout->addWidget(this->lblCurrentColor);

    this->setLayout(colorLayout);
}

QColor ColorConfiguration::getColor()
{
    return this->color;
}

void ColorConfiguration::setColor(QColor color)
{
    this->color = color;
}

void ColorConfiguration::onChooseButtonClicked()
{
    this->setColor(QColorDialog::getColor());
    setLabelColor(this->getColor());
}

void ColorConfiguration::setLabelColor(QColor color)
{
    QPalette palette = this->lblCurrentColor->palette();
    palette.setColor(this->lblCurrentColor->backgroundRole(), color);
    palette.setColor(this->lblCurrentColor->foregroundRole(), color);
    this->lblCurrentColor->setPalette(palette);
}

