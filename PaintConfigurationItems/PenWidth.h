#ifndef PENWIDTH_CONFIGURATION_H
#define PENWIDTH_CONFIGURATION_H

#include <QDoubleSpinBox>

class PenWidthConfiguration : public QDoubleSpinBox
{
    Q_OBJECT
public:
    explicit PenWidthConfiguration(QWidget *parent = 0);

    double getValue();
};

#endif // PENWIDTH_CONFIGURATION_H
