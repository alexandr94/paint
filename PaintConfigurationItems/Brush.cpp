#include "Brush.h"

BrushConfiguration::BrushConfiguration(QWidget *parent) :
    QComboBox(parent)
{
    this->addItem("Solid", QVariant(Brushes::SOLID_LINE));
    this->addItem("Points", QVariant(Brushes::POINTS_LINE));
    this->addItem("Horizontal line", QVariant(Brushes::HORIZONTAL_LINE));
    this->addItem("Vertical line", QVariant(Brushes::VERTIVAL_LINE));

    this->setBrush(QBrush(Qt::SolidPattern));

    connect(this, SIGNAL(currentIndexChanged(int)), this, SLOT(onBrushChanged(int)));
}

void BrushConfiguration::onBrushChanged(int index)
{
    switch (index) {

    case Brushes::POINTS_LINE:
        this->setBrush(QBrush(Qt::Dense7Pattern));
        break;

    case Brushes::HORIZONTAL_LINE:
        this->setBrush(QBrush(Qt::HorPattern));
        break;

    case Brushes::VERTIVAL_LINE:
        this->setBrush(QBrush(Qt::VerPattern));
        break;

    case Brushes::SOLID_LINE:
        this->setBrush(QBrush(Qt::SolidPattern));
        break;
    }
}

int BrushConfiguration::getValue()
{
    return this->currentData().toInt();
}

QBrush BrushConfiguration::getBrush()
{
    return this->brush;
}

void BrushConfiguration::setBrush(QBrush brush)
{
    this->brush = brush;
}

