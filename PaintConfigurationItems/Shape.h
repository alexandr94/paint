#ifndef SHAPE_CONFIGURATION_H
#define SHAPE_CONFIGURATION_H

#include <QComboBox>
#include <QDebug>

#include "ShapePainters/Shapes.h"

class ShapeConfiguration : public QComboBox
{
    Q_OBJECT
public:
    explicit ShapeConfiguration(QWidget *parent = 0);

    int getValue();

signals:
    void shapeChangeEvent(int shape);

protected slots:
    void onShapeChanged(int index);

};

#endif // SHAPE_CONFIGURATION_H
