#ifndef COLOR_H
#define COLOR_H

#include <QColorDialog>
#include <QPushButton>
#include <QLabel>
#include <QPalette>
#include <QHBoxLayout>


class ColorConfiguration : public QWidget
{
    Q_OBJECT
public:
    explicit ColorConfiguration(QWidget *parent = 0);

    QColor getColor();
    void setColor(QColor color);

signals:

public slots:
    void onChooseButtonClicked();

protected:
    void setLabelColor(QColor color);

    QColor color;
    QLabel *lblCurrentColor;

};

#endif // COLOR_H
